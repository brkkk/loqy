PREFIX = /usr/local

X11INC = /usr/X11R6/include
X11LIB = /usr/X11R6/lib

# includes and libs
INCS = -I/usr/include/freetype2 -I${X11INC}
LIBS = -L/usr/lib -lc -lcrypt -L${X11LIB} -lX11 -lXxf86vm -lXft

CC = gcc
CFLAGS = -O2 ${INCS} ${CPPFLAGS} # -pedantic -Wall
LDFLAGS = ${LIBS}

all: loqy


loqy:
	${CC} main.c ${CFLAGS} ${LDFLAGS} -o loqy

install: all
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f loqy ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/loqy
	@chmod u+s ${DESTDIR}${PREFIX}/bin/loqy

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/loqy
